import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class ContactServiceMockTest {

    @Mock
    private IContactDao contactDao;

    @InjectMocks
    private ContactServiceImpl contactService = new ContactServiceImpl();

    @Captor
    ArgumentCaptor<Contact> contactCaptor;

    @Test(expected = ContactException.class)
    public void shouldFailOnDuplicatEntry() throws ContactException {
        // def du mock
        Mockito.when(contactDao.exists("Thierry")).thenReturn(true);
        // Test
        this.contactService.creerContact("Thierry");
    }

    @Test
    public void shouldPass() throws ContactException{
        Mockito.when(contactDao.exists("Thierry")).thenReturn(false);

        contactService.creerContact("Thierry");

        Mockito.verify(contactDao).addContact(contactCaptor.capture());

        Contact contact = contactCaptor.getValue();
        assertEquals("Thierry",contact.getNom());
    }


//    @Test(expected = ContactException.class)
//    public void shouldFailIfNull() throws ContactException {
//        // def du mock
//        Mockito.when(contactDao.exists("Thierry")).thenReturn(true);
//        // Test
//        this.contactService.creerContact("Thierry");
//    }
}
