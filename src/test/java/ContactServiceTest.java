import org.junit.Test;

public class ContactServiceTest {
    IContactService service = new ContactServiceImpl();

    @Test (expected = ContactException.class)
    public void shouldFailIfNull() throws ContactException {
        service.creerContact(null);
    }

    @Test (expected = ContactException.class)
    public void shouldFailIfSizeEqual0() throws ContactException {
        service.creerContact("");
    }

    @Test (expected = ContactException.class)
    public void shouldFailIfSizeLowerThan3() throws ContactException {
        service.creerContact("AB");
    }

    @Test (expected = ContactException.class)
    public void shouldFailIfSizeBiggerThan40() throws ContactException {
        StringBuilder str = new StringBuilder();
        for(int i = 0; i < 41; i++){
            str.append("a");
        }
        service.creerContact(str.toString());
    }
}
