public interface IContactDao {
    boolean exists(String name);

    void addContact(Contact contact);

    void removeContact(Contact contact);
}
