import java.util.ArrayList;
import java.util.List;

public class ContactDao implements IContactDao{

    private List<Contact> contacts = new ArrayList<>();

    public ContactDao() {
    }

    public boolean exists(String name){
        boolean ret = false;
        for (int i =0; i < contacts.size(); i++){
            if (name.equalsIgnoreCase(contacts.get(i).getNom())){
                ret = true;
            }
        }
        return ret;
    }

    public void addContact(Contact contact){
        contacts.add(contact);
    }

    public void removeContact(Contact contact){
        for (Contact currentContact: contacts){
            if (currentContact == contact){
                contacts.remove(contact);
            }
        }
    }
}
