public class ContactException extends Exception {

    public ContactException(String errorMessage){
        super(errorMessage);
    }
}
