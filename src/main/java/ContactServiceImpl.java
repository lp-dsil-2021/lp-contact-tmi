public class ContactServiceImpl implements IContactService {

    private IContactDao contactDao = new ContactDao();

    public Contact creerContact(String nom) throws ContactException {
        if (nom == null || nom.length() < 3 || nom.length() > 40){
            throw new ContactException("Nom incorrect");
        }else if (contactDao.exists(nom)){
            throw new ContactException("Nom déja utilisé");
        }
        Contact contact = new Contact();
        contact.setNom(nom);
        contactDao.addContact(contact);
        return contact;
    }
}
